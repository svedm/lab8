﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.IO;
using System.Web.Script.Serialization;
using NoSQLServer.Common;

namespace NoSQLServer
{
	[ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
	public class Service : IService
	{
		public static Data Data { get; set; }
		public static string FileName { get; set; }

		public Service(int port)
		{
			switch (port)
			{
				case 1001: FileName = "Data1.json";
					break;
				case 1002: FileName = "Data2.json";
					break;
				case 1003: FileName = "Data3.json";
					break;
				default: return;
			}

			var fileContent = File.ReadAllText(FileName, Encoding.GetEncoding(1251));
			Data = new JavaScriptSerializer().Deserialize<Data>(fileContent);
		}

		public IEnumerable<KeyValue> GetNumber(string sort)
		{
			return Reduce.Exec(Data.Orders.SelectMany(order => order.Items, (order, item) => new { order, item })
				.Where(t => t.item.Name == sort)
				.Select(t => new KeyValue
				{
					Key = t.item.Name,
					Value = t.item.Number
				}));
		}

		public IEnumerable<KeyValue> GetPrice(string sort)
		{
			return Reduce.Exec(Data.Orders.SelectMany(order => order.Items, (order, item) => new { order, item })
				.Where(t => t.item.Name == sort)
				.Select(t => new KeyValue
				{
					Key = t.item.Name,
					Value = t.item.Price * t.item.Number
				}));
		}

		public IEnumerable<KeyValue> GetNumberOfBottlesInCity(string cityName)
		{
			return Reduce.Exec(Data.Orders.Where(order => order.City == cityName)
				.SelectMany(order => order.Items, (order, item) =>
					new KeyValue
					{
						Key = item.Name,
						Value = item.Number
					}));
		}
	}
}
