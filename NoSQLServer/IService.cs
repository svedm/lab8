using System.Collections.Generic;
using System.ServiceModel;
using NoSQLServer.Common;

namespace NoSQLServer
{
	[ServiceContract]
	public interface IService
	{
		[OperationContract]
		IEnumerable<KeyValue> GetNumber(string sort);

		[OperationContract]
		IEnumerable<KeyValue> GetPrice(string sort);

		[OperationContract]
		IEnumerable<KeyValue> GetNumberOfBottlesInCity(string cityName);
	}
}