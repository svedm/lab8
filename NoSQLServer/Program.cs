﻿using System;

namespace NoSQLServer
{
    class Program
    {
        static void Main(string[] args)
        {
	        if (args.Length == 0) return;

	        var port = int.Parse(args[0]);
	        var server = new MyServer(port, false);
	        server.Open();
	        Console.ReadKey();
	        server.Close();
        }
    }
}
