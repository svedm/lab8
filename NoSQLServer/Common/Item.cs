﻿using System.Runtime.Serialization;

namespace NoSQLServer.Common
{
	[DataContract]
	public class Item
	{
		[DataMember(Name = "name")]
		public string Name { get; set; }

		[DataMember(Name = "price")]
		public double Price { get; set; }

		[DataMember(Name = "number")]
		public int Number { get; set; }
	}
}