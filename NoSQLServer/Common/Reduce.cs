using System.Collections.Generic;
using System.Linq;

namespace NoSQLServer.Common
{
	public static class Reduce
	{
		public static IEnumerable<KeyValue> Exec(IEnumerable<KeyValue> input)
		{
			return from s in input
				group s by s.Key into grp
				select new KeyValue
				{
					Key = grp.Key,
					Value = grp.Sum(item => item.Value)
				};
		}
	}
}