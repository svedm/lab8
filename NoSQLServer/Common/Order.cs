﻿using System.Runtime.Serialization;

namespace NoSQLServer.Common
{
	[DataContract]
	public class Order
	{
		[DataMember(Name = "id")]
		public int Id { get; set; }

		[DataMember(Name = "customer")]
		public string Customer { get; set; }

		[DataMember(Name = "city")]
		public string City { get; set; }

		[DataMember(Name = "listItems")]
		public Item[] Items { get; set; }
	}
}