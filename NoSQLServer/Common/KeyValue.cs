using System.Runtime.Serialization;

namespace NoSQLServer.Common
{
	[DataContract]
	public class KeyValue
	{
		[DataMember]
		public string Key;

		[DataMember]
		public double Value;
	}
}