﻿using System.Runtime.Serialization;

namespace NoSQLServer.Common
{
    [DataContract]
    public class Data
    {
        [DataMember(Name = "orders")]
        public Order[] Orders { get; set; }
    }
}
