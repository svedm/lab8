﻿using System;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace NoSQLServer
{
	public class MyServer
	{
		public readonly int Port;
		private readonly bool _debug;

		private readonly ServiceHost _host;

		public MyServer(int port, bool debug)
		{
			Port = port;
			_debug = debug;

			var service = new Service(port);

			_host = _debug
				? new ServiceHost(service,
						new Uri(String.Format("http://localhost:{0}", Port+10)),
						new Uri(String.Format("net.tcp://localhost:{0}", Port)))
				: new ServiceHost(service,
					new Uri(String.Format("net.tcp://localhost:{0}", Port)));
		}

		public void Open()
		{
			_host.AddServiceEndpoint(typeof(IService), new NetTcpBinding(), "");

			if (_debug)
			{
				_host.AddServiceEndpoint(typeof(IService), new BasicHttpBinding(), "");

				var smb = new ServiceMetadataBehavior { HttpGetEnabled = true };
				_host.Description.Behaviors.Add(smb);
			}
			_host.Faulted += (sender, args) => Console.WriteLine("Error: {0}", String.Join(" ", args));
			_host.Opened += (sender, args) => Console.WriteLine("Host opened: {0}", String.Join(" ", args));
			_host.UnknownMessageReceived += (sender, args) => Console.WriteLine("Unknown message: {0}", args);

			_host.Open();
			foreach (var endpt in _host.Description.Endpoints)
			{
				Console.WriteLine("Enpoint address:\t{0}", endpt.Address);
				Console.WriteLine("Enpoint binding:\t{0}", endpt.Binding);
				Console.WriteLine("Enpoint contract:\t{0}\n", endpt.Contract.ContractType.Name);
			}
		}

		public void Close()
		{
			_host.Close();
		}
	}
}
