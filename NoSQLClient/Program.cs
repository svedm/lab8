﻿using System;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;
using NoSQLClient.ServiceReference1;
using NoSQLServer;
using NoSQLServer.Common;

namespace NoSQLClient
{
	class Program
	{
		static void Main(string[] args)
		{
			//var p1 = Process.Start(@"..\..\..\NoSQLServer\bin\Debug\NoSQLServer.exe", "1001");
			//var p2 = Process.Start(@"..\..\..\NoSQLServer\bin\Debug\NoSQLServer.exe", "1002");
			//var p3 = Process.Start(@"..\..\..\NoSQLServer\bin\Debug\NoSQLServer.exe", "1003");
			var server1 = new MyServer(1001, false);
			var server2 = new MyServer(1002, false);
			var server3 = new MyServer(1003, false);

			server1.Open();
			server2.Open();
			server3.Open();

			var client1 = new ServiceClient(new NetTcpBinding(), new EndpointAddress("net.tcp://localhost:1001"));
			var client2 = new ServiceClient(new NetTcpBinding(), new EndpointAddress("net.tcp://localhost:1002"));
			var client3 = new ServiceClient(new NetTcpBinding(), new EndpointAddress("net.tcp://localhost:1003"));

			Console.WriteLine("*****MapReduce*****");

			do
			{
				Console.WriteLine();
				Console.WriteLine("Выберите действие, нажав соответствующую цифру");
				Console.WriteLine("1-Получить количество заказанных напитков определенного типа");
				Console.WriteLine("2-Получить суммарную стоимость всех заказанных напитков определенного типа");
				Console.WriteLine("3-Получить количество заказанных в определенный город напитков");
				Console.WriteLine("4-Выйти из программы");

				int a;
				do
				{
					Console.WriteLine();
					Console.Write("Введите цифру: ");
					try
					{
						a = Convert.ToInt32(Console.ReadLine());
						if ((a >= 1) && (a <= 4)) { break; }
						Console.WriteLine();
						Console.WriteLine("Неправильно введено число, попробуйте еще раз!");
					}
					catch
					{
						Console.WriteLine();
						Console.WriteLine("Неправильно введено число, попробуйте еще раз!");
					}
				} while (true);

				if (a == 4)
				{
					server1.Close();
					server2.Close();
					server3.Close();
					//client1.Close();
					//if (p1 != null) p1.CloseMainWindow();

					//client2.Close();
					//if (p2 != null) p2.CloseMainWindow();

					//client3.Close();
					////if (p3 != null) p3.CloseMainWindow();
					return;
				}

				switch (a)
				{
					case 1:
						{
							Console.Write("Введите название напитка - Tea или Coffee: ");
							var sort = Console.ReadLine();
							Console.WriteLine();
							var t1 = Task.Run(() => client1.GetNumber(sort));
							var t2 = Task.Run(() => client2.GetNumber(sort));
							var t3 = Task.Run(() => client3.GetNumber(sort));

							var result = t1.Result.Concat(t2.Result).Concat(t3.Result);
							result = Reduce.Exec(result);

							if (result.ToArray().Length == 0) Console.WriteLine("Нет таких заказов!");
							else
								foreach (var r in result)
								{
									Console.Write("Всего заказано " + sort + ": ");
									Console.WriteLine(r.Value);
								}
						}
						break;
					case 2:
						{
							Console.Write("Введите название напитка - Tea или Coffee: ");
							var sort = Console.ReadLine();
							Console.WriteLine();

							var t1 = Task.Run(() => client1.GetPrice(sort));
							var t2 = Task.Run(() => client2.GetPrice(sort));
							var t3 = Task.Run(() => client3.GetPrice(sort));

							var result = t1.Result.Concat(t2.Result).Concat(t3.Result);
							result = Reduce.Exec(result);

							if (result.ToArray().Length == 0) Console.WriteLine("Нет таких заказов!");
							else
								foreach (var r in result)
								{
									Console.Write("Всего заказано " + sort + " на сумму: ");
									Console.WriteLine(r.Value);
								}
						}
						break;
					case 3:
						{
							Console.Write("Введите название города: ");
							var city = Console.ReadLine();
							Console.WriteLine();

							var t1 = Task.Run(() => client1.GetNumberOfBottlesInCity(city));
							var t2 = Task.Run(() => client2.GetNumberOfBottlesInCity(city));
							var t3 = Task.Run(() => client3.GetNumberOfBottlesInCity(city));

							var result = t1.Result.Concat(t2.Result).Concat(t3.Result);
							result = Reduce.Exec(result);

							if (result.ToArray().Length == 0) Console.WriteLine("Нет заказов в указанный город!");
							else
								foreach (var r in result)
								{
									Console.Write("Всего заказано " + r.Key + ": ");
									Console.WriteLine(r.Value);
								}
						}
						break;
				}

				Console.WriteLine();
				Console.WriteLine("Для продолжения нажмите любую клавишу");
				Console.ReadKey();
			} while (true);


		}
	}
}
